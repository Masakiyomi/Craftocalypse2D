package com.gmail.masakiyoomi.craftocalypse2d.equipment;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.security.InvalidParameterException;

@RunWith(JUnit4.class)
public class ItemStackTest {

    @Test(expected = InvalidParameterException.class)
    public void createItemStackWithNegativeAmount() {
        new ItemStack(Item.FEATHER, -1);
    }

    @Test
    public void basicAmountAssignment() {
        ItemStack is = new ItemStack(Item.COIN);
        Assert.assertEquals(1, is.getAmount());
    }

    @Test
    public void setAmountToItemStack() {
        ItemStack is = new ItemStack(Item.COIN, 50);
        is.setAmount(89);
        Assert.assertEquals(89, is.getAmount());
    }

    @Test
    public void getItemStack() {
        ItemStack is = new ItemStack(Item.FEATHER, 50);
        Assert.assertNotNull(is.getItem());
    }
}
