package com.gmail.masakiyoomi.craftocalypse2d.equipment;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class EquipmentTest {
    @Test
    public void setItemStackWithNormalData() {
        ItemStack is = new ItemStack(Item.FEATHER);
        Equipment eq = new Equipment(15);
        eq.setItemStack(1, is);
        Assert.assertEquals(eq.getItemStackAt(1), is);
    }

    @Test
    public void setTheSameItemStackWithNormalDataTwice() {
        ItemStack is = new ItemStack(Item.FEATHER);
        Equipment eq = new Equipment(15);
        eq.setItemStack(1, is);
        eq.setItemStack(2, is);
        Assert.assertEquals(eq.getItemStackAt(1), eq.getItemStackAt(2));
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void setItemStackOnNegativeIndex() {
        ItemStack is = new ItemStack(Item.FEATHER);
        Equipment eq = new Equipment(15);
        eq.setItemStack(-10, is);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void setItemStackOnTooBigIndex() {
        ItemStack is = new ItemStack(Item.FEATHER);
        Equipment eq = new Equipment(50);
        eq.setItemStack(100, is);
    }

    @Test
    public void addItemStackToEmptyEquipment() {
        ItemStack is = new ItemStack(Item.FEATHER, 60);
        Equipment eq = new Equipment(10);
        eq.addItemStack(is);
        Assert.assertEquals(60, eq.getItemStackAt(0).getAmount());
    }

    @Test
    public void addMultipleItemStacksToEmptyEquipment() {
        ItemStack is = new ItemStack(Item.FEATHER);
        Equipment eq = new Equipment(10);
        eq.addItemStacks(is, 4);
        Assert.assertEquals(1, eq.getItemStackAt(0).getAmount());
        Assert.assertEquals(1, eq.getItemStackAt(1).getAmount());
        Assert.assertEquals(1, eq.getItemStackAt(2).getAmount());
        Assert.assertEquals(1, eq.getItemStackAt(3).getAmount());

    }

    @Test
    public void addMultipleFullItemStacksToEmptyEquipment() {
        ItemStack is = new ItemStack(Item.FEATHER);
        int stackSize = is.getItem().getStackSize();
        Equipment eq = new Equipment(10);
        eq.addFullItemStacks(is, 4);
        Assert.assertEquals(stackSize, eq.getItemStackAt(0).getAmount());
        Assert.assertEquals(stackSize, eq.getItemStackAt(1).getAmount());
        Assert.assertEquals(stackSize, eq.getItemStackAt(2).getAmount());
        Assert.assertEquals(stackSize, eq.getItemStackAt(3).getAmount());

    }

    @Test
    public void addMultiplePartialItemStacksToEmptyEquipment() {
        ItemStack is = new ItemStack(Item.FEATHER, 50);
        Equipment eq = new Equipment(10);
        eq.addItemStacks(is, 4);
        Assert.assertEquals(50, eq.getItemStackAt(0).getAmount());
        Assert.assertEquals(50, eq.getItemStackAt(1).getAmount());
        Assert.assertEquals(50, eq.getItemStackAt(2).getAmount());
        Assert.assertEquals(50, eq.getItemStackAt(3).getAmount());
    }

    @Test
    public void addPartialItemStackToNonEmptyEquipment() {
        ItemStack is = new ItemStack(Item.FEATHER, 3);
        Equipment eq = new Equipment(10);
        eq.setItemStack(4, new ItemStack(Item.FEATHER, 99));
        eq.setItemStack(5, new ItemStack(Item.FEATHER, 99));
        eq.addItemStack(is);
        Assert.assertEquals(100, eq.getItemStackAt(4).getAmount());
        Assert.assertEquals(100, eq.getItemStackAt(5).getAmount());
        Assert.assertEquals(1, eq.getItemStackAt(0).getAmount());
    }

    @Test
    public void addDifferentPartialItemStackToNonEmptyEquipment() {
        ItemStack is1 = new ItemStack(Item.FEATHER, 3);
        ItemStack is2 = new ItemStack(Item.FEATHER, 8);
        ItemStack is3 = new ItemStack(Item.COIN, 3);
        Equipment eq = new Equipment(10);
        eq.setItemStack(4, is1);
        eq.setItemStack(6, is2);
        eq.addItemStack(is3);
        Assert.assertEquals(is1, eq.getItemStackAt(4));
        Assert.assertEquals(is2, eq.getItemStackAt(6));
        Assert.assertEquals(is3, eq.getItemStackAt(0));
    }

    @Test
    public void addTooBigItemStack() {
        ItemStack is = new ItemStack(Item.COIN, 752);
        Equipment eq = new Equipment(8);
        eq.addItemStack(is);
        Assert.assertEquals(100, eq.getItemStackAt(0).getAmount());
        Assert.assertEquals(100, eq.getItemStackAt(1).getAmount());
        Assert.assertEquals(100, eq.getItemStackAt(2).getAmount());
        Assert.assertEquals(100, eq.getItemStackAt(3).getAmount());
        Assert.assertEquals(100, eq.getItemStackAt(4).getAmount());
        Assert.assertEquals(100, eq.getItemStackAt(5).getAmount());
        Assert.assertEquals(100, eq.getItemStackAt(6).getAmount());
        Assert.assertEquals(52, eq.getItemStackAt(7).getAmount());
    }

    @Test
    public void addItemStackToMakeItemsOverflow() {
        ItemStack is = new ItemStack(Item.COIN, 150);
        Equipment eq = new Equipment(1);
        Assert.assertFalse(eq.addItemStack(is));
    }

    @Test
    public void getEquipment() {
        Equipment eq = new Equipment(15);
        Assert.assertNotNull(eq.getEquipment());
    }

    @Test
    public void equipmentAtOneEqualsEquipmentAtTwo() {
        Equipment eq1 = new Equipment(15);
        Equipment eq2 = new Equipment(15);
        Assert.assertEquals(eq1, eq2);
    }

    @Test
    public void addSmallAmountToPartialItemStack() {
        Equipment eq1 = new Equipment(15);
        eq1.setItemStack(0, new ItemStack(Item.COIN, 50));
        eq1.addItemStack(new ItemStack(Item.COIN, 3));
        Assert.assertEquals(53, eq1.getItemStackAt(0).getAmount());
    }
}
