package com.gmail.masakiyoomi.craftocalypse2d.equipment;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class ItemTest {

    @Test
    public void getItemName() {
        Item gloves = Item.LEATHER_GLOVES;
        Assert.assertNotNull(gloves.getName());
    }

    @Test
    public void getItemMetaData() {
        Item gloves = Item.LEATHER_GLOVES;
        Assert.assertNotNull(gloves.getMetaData());
    }

    @Test
    public void getItemShopValue() {
        Item gloves = Item.LEATHER_GLOVES;
        Assert.assertNotNull(gloves.getShopValue());
    }

    @Test
    public void getItemStackSize() {
        Item gloves = Item.LEATHER_GLOVES;
        Assert.assertNotNull(gloves.getStackSize());
    }

    @Test
    public void getItemSellValueMultiplier() {
        Item gloves = Item.LEATHER_GLOVES;
        Assert.assertNotNull(gloves.getSellValueMultiplier());
    }

    @Test
    public void itemEquals() {
        Item gloves = Item.LEATHER_GLOVES;
        Assert.assertFalse(gloves.equals(Item.LEATHER_SKIRT));
    }

}
