package com.gmail.masakiyoomi.craftocalypse2d.equipment;

import com.gmail.masakiyoomi.craftocalypse2d.map.Position;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class ItemEntityTest {

    @Test
    public void getItemEntityItemStack() {
        ItemEntity itemEntity = new ItemEntity(new ItemStack(Item.LEATHER_SKIRT), new Position(5, 10));
        Assert.assertNotNull(itemEntity.getItemStack());
    }

    @Test
    public void getItemEntityCoordinates() {
        ItemEntity itemEntity = new ItemEntity(new ItemStack(Item.LEATHER_SKIRT), new Position(5, 10));
        Assert.assertEquals(5, itemEntity.getPosition().getX());
        Assert.assertEquals(10, itemEntity.getPosition().getY());
    }

    @Test
    public void itemEntityEquals() {
        ItemEntity itemEntity01 = new ItemEntity(new ItemStack(Item.LEATHER_SKIRT), new Position(5, 10));
        ItemEntity itemEntity02 = new ItemEntity(new ItemStack(Item.LEATHER_JACKET), new Position(8, 3));
        Assert.assertFalse(itemEntity01.equals(itemEntity02));
    }

}
