package com.gmail.masakiyoomi.craftocalypse2d;

import com.gmail.masakiyoomi.craftocalypse2d.entity.*;
import com.gmail.masakiyoomi.craftocalypse2d.equipment.Equipment;
import com.gmail.masakiyoomi.craftocalypse2d.equipment.Item;
import com.gmail.masakiyoomi.craftocalypse2d.equipment.ItemEntity;
import com.gmail.masakiyoomi.craftocalypse2d.equipment.ItemStack;
import com.gmail.masakiyoomi.craftocalypse2d.map.Landscape;
import com.gmail.masakiyoomi.craftocalypse2d.map.Position;

public class Main {
    private static GameInterface gameInterface;

    public static void main(String[] args) {
        init();
        while (true) {
            loop();
        }
    }

    public static void init() {
        Landscape mainland = new Landscape("Mainland", 100);
        Player masakiyomi = new Player("Masakiyomi", new Position(50, 50), new Equipment(30), 100, 100);
        mainland.add(masakiyomi);
        masakiyomi.getEquipment().addItemStack(new ItemStack(Item.WOODEN_SWORD));
        gameInterface = new ConsoleGameInterface(masakiyomi, mainland);
        mainland.add(new ItemEntity(new ItemStack(Item.WOODEN_SHIELD), new Position(51, 50)));
        mainland.add(new ItemEntity(new ItemStack(Item.WOODEN_SWORD), new Position(52, 50)));
        mainland.add(new ItemEntity(new ItemStack(Item.COIN, 10), new Position(55, 52)));
        mainland.add(new Zombie(54, 52));
    }

    public static void loop() {
        gameInterface.processInput();
        gameInterface.updateGameObjects();
        gameInterface.render();
    }
}