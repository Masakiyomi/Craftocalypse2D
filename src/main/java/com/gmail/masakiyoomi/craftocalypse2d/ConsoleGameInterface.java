package com.gmail.masakiyoomi.craftocalypse2d;

import com.gmail.masakiyoomi.craftocalypse2d.entity.Entity;
import com.gmail.masakiyoomi.craftocalypse2d.entity.LivingEntity;
import com.gmail.masakiyoomi.craftocalypse2d.entity.Player;
import com.gmail.masakiyoomi.craftocalypse2d.equipment.Item;
import com.gmail.masakiyoomi.craftocalypse2d.equipment.ItemEntity;
import com.gmail.masakiyoomi.craftocalypse2d.equipment.ItemStack;
import com.gmail.masakiyoomi.craftocalypse2d.map.Landscape;
import com.gmail.masakiyoomi.craftocalypse2d.map.Position;
import lombok.Getter;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.security.InvalidParameterException;
import java.util.Scanner;

public class ConsoleGameInterface implements GameInterface {
    private Player player;
    private Landscape landscape;
    String answer;
    private int dx; //for player movement
    private int dy; //for player movement

    public ConsoleGameInterface(Player player, Landscape landscape) {
        this.player = player;
        this.landscape = landscape;
    }

    public boolean setCgiLandscape(Landscape landscape) {
        this.landscape = landscape;
        return true;
    }

    public void movePlayer(int dx, int dy){
        int x = player.getPosition().getX();
        int y = player.getPosition().getY();
        player.setPosition(new Position(x + dx, y + dy));
    }

    @Override
    public void processInput() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Where would you like to move? (Up, Down, Left, Right)");
        answer = scanner.nextLine();
    }

    @Override
    public void updateGameObjects(){
        if (answer.equalsIgnoreCase("Up")){
            dx = 0;
            dy = -1;
        }

        else if (answer.equalsIgnoreCase("Down")){
            dx = 0;
            dy = 1;
        }

        else if (answer.equalsIgnoreCase("Right")){
            dx = 1;
            dy = 0;
        }

        else if (answer.equalsIgnoreCase("Left")){
            dx = -1;
            dy = 0;
        }

        if(dx != 0 || dy != 0){
            Entity targetEntity = landscape.getEntity(player.getPosition().getX() + dx, player.getPosition().getY() + dy);

            if (targetEntity instanceof ItemEntity) {
                Item item = ((ItemEntity) targetEntity).getItemStack().getItem();
                int amount = ((ItemEntity) targetEntity).getItemStack().getAmount();
                player.getEquipment().addItemStack(new ItemStack(item, amount));
                landscape.removeEntity(targetEntity);
            }

            else if (targetEntity instanceof LivingEntity){
                System.out.println("DEBUG - You aren't allowed to go there. Reason: LivingEntity has already occupied these coordinates.");
                dx = 0;
                dy = 0;
            }

            movePlayer(dx, dy);
            System.out.println("DEBUG - Current User position: " + player.getPosition());
        }
    }

    @Override
    public void render() {
    }
}