package com.gmail.masakiyoomi.craftocalypse2d.equipment;

import lombok.Getter;

import java.security.InvalidParameterException;
import lombok.Setter;

public class ItemStack {
    @Getter
    @Setter
    private Item item;
    @Getter
    private int amount;

    public ItemStack(Item item) {
        this.item = item;
        amount = 1;
    }

    public ItemStack(Item item, int amount) {
        this(item);
        setAmount(amount);
    }

    public void setAmount(int amount) {
        if (amount < 0) {
            throw new InvalidParameterException("Amount cannot be negative");
        }
        this.amount = amount;
    }
}
