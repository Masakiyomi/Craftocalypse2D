package com.gmail.masakiyoomi.craftocalypse2d.equipment;

import lombok.Value;
import lombok.experimental.NonFinal;

@Value
public class Equipment {
    @NonFinal
    private ItemStack[] equipment;

    public Equipment(int capacity) {
        equipment = new ItemStack[capacity];
    }

    public boolean addItemStack(ItemStack itemStack) {
        int amount = itemStack.getAmount();
        int stackSize = itemStack.getItem().getStackSize();

        for (int i = 0; i < equipment.length; i++) {
            if (equipment[i] != null) {
                if (equipment[i].getItem().equals(itemStack.getItem()) && equipment[i].getAmount() < stackSize) {
                    int firstAmount = equipment[i].getAmount();
                    if (firstAmount + amount <= stackSize) {
                        equipment[i].setAmount(firstAmount + amount);
                        return true;
                    } else {
                        equipment[i].setAmount(stackSize);
                        amount -= (stackSize - firstAmount);
                    }
                }
            }
        }

        if (amount > 0) {
            for (int j = 0; j < equipment.length; j++) {
                if (equipment[j] == null) {
                    equipment[j] = itemStack;
                    if (amount <= stackSize) {
                        equipment[j].setAmount(amount);
                        return true;
                    } else {
                        equipment[j].setAmount(stackSize);
                        itemStack = new ItemStack(itemStack.getItem(), amount -= stackSize);
                    }
                }
            }
        }
        return false;
    }

    public boolean addItemStacks(ItemStack itemStack, int amount) {
        int counter = 0;
        for (int j = 0; j < equipment.length; j++) {
            if (equipment[j] == null && counter < amount) {
                equipment[j] = itemStack;
                itemStack = new ItemStack(itemStack.getItem(), itemStack.getAmount());
                counter++;
            }
        }
        return true;
    }

    public boolean addFullItemStacks(ItemStack itemStack, int amount) {
        int counter = 0;
        int stackSize = itemStack.getItem().getStackSize();
        for (int j = 0; j < equipment.length; j++) {
            if (equipment[j] == null && counter < amount) {
                equipment[j] = itemStack;
                equipment[j].setAmount(stackSize);
                itemStack = new ItemStack(itemStack.getItem(), stackSize);
                counter++;
            }
        }
        return true;
    }

    public void setItemStack(int index, ItemStack itemStack) {
        equipment[index] = itemStack;
    }

    public ItemStack getItemStackAt(int index) {
        return equipment[index];
    }

}