package com.gmail.masakiyoomi.craftocalypse2d.equipment;

import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.NonFinal;

@Value
public class Item {
    private final String name;
    private final MetaData metaData;
    @NonFinal
    private int shopValue;
    private final double sellValueMultiplier = .5;
    private int stackSize;
    public static final Item LEATHER_HELMET = new Item("Leather helmet", new ArmorMetaData(2), 20, 1);
    public static final Item LEATHER_JACKET = new Item("Leather jacket",new ArmorMetaData(4), 30, 1);
    public static final Item LEATHER_LEGGINGS = new Item("Leather leggings",new ArmorMetaData(3), 25, 1);
    public static final Item LEATHER_SKIRT = new Item("Leather helmet",new ArmorMetaData(3), 25, 1);
    public static final Item LEATHER_BOOTS = new Item("Leather helmet",new ArmorMetaData(2), 20, 1);
    public static final Item LEATHER_GLOVES = new Item("Leather helmet",new ArmorMetaData(1), 10, 1);
    public static final Item HARDENED_LEATHER_HELMET = new Item("Hardened leather helmet",new ArmorMetaData(3), 28, 1);
    public static final Item HARDENED_LEATHER_JACKET = new Item("Hardened leather jacket",new ArmorMetaData(6), 42, 1);
    public static final Item HARDENED_LEATHER_LEGGINGS = new Item("Hardened leather leggings",new ArmorMetaData(5), 37, 1);
    public static final Item HARDENED_LEATHER_SKIRT = new Item("Hardened leather skirt",new ArmorMetaData(5), 37, 1);
    public static final Item HARDENED_LEATHER_BOOTS = new Item("Hardened leather boots",new ArmorMetaData(3), 28, 1);
    public static final Item HARDENED_LEATHER_GLOVES = new Item("Hardened leather gloves",new ArmorMetaData(2), 17, 1);
    public static final Item IRON_HELMET = new Item("Iron helmet", new ArmorMetaData(5), 40, 1);
    public static final Item IRON_BREASTPLATE = new Item("Iron breastplate",new ArmorMetaData(9), 58, 1);
    public static final Item IRON_LEGGINGS = new Item("Iron leggings",new ArmorMetaData(7), 50, 1);
    public static final Item IRON_BOOTS = new Item("Iron boots",new ArmorMetaData(5), 40, 1);
    public static final Item IRON_GLOVES = new Item("Iron gloves",new ArmorMetaData(4), 30, 1);
    public static final Item STEEL_HELMET = new Item("Steel helmet", new ArmorMetaData(8), 60, 1);
    public static final Item STEEL_IRON_BREASTPLATE = new Item("Steel breastplate",new ArmorMetaData(12), 86, 1);
    public static final Item STEEL_LEGGINGS = new Item("Steel leggings",new ArmorMetaData(10), 72, 1);
    public static final Item STEEL_BOOTS = new Item("Steel boots",new ArmorMetaData(8), 60, 1);
    public static final Item STEEL_GLOVES = new Item("Steel gloves",new ArmorMetaData(6), 42, 1);
    public static final Item HELMET_OF_DAMNATION = new Item("Helmet of Damnation",new ArmorMetaData(25), 50000000, 1);
    public static final Item JACKET_OF_DAMNATION = new Item("Jacket of Damnation",new ArmorMetaData(62), 50000000, 1);
    public static final Item LEGGINGS_OF_DAMNATION = new Item("Leggings of Damnation",new ArmorMetaData(48), 50000000, 1);
    public static final Item SKIRT_OF_DAMNATION = new Item("Skirt of Damnation",new ArmorMetaData(48), 50000000, 1);
    public static final Item BOOTS_OF_DAMNATION = new Item("Boots of Damnation",new ArmorMetaData(34), 50000000, 1);
    public static final Item GLOVES_OF_DAMNATION = new Item("Gloves of Damnation",new ArmorMetaData(27), 50000000, 1);
    public static final Item WOOD_LOG = new Item("Wood log",new ShieldMetaData(5), 5, 1);
    public static final Item WOODEN_SHIELD = new Item("Wooden shield",new ShieldMetaData(10), 30, 1);
    public static final Item STEEL_SHIELD = new Item("Steel shield",new ShieldMetaData(13), 48, 1);
    public static final Item SHIELD_OF_DAMNATION = new Item("Shield of Damnation",new ShieldMetaData(50), 50000000, 1);
    public static final Item WOODEN_SWORD = new Item("Wooden sword",new SwordMetaData(10, 1.5), 30, 1);
    public static final Item STONE_SWORD = new Item("Stone sword",new SwordMetaData(12, 1.5), 45, 1);
    public static final Item STEEL_SWORD = new Item("Steel sword",new SwordMetaData(15, 1.5), 65, 1);
    public static final Item SWORD_OF_DAMNATION = new Item("Sword of Damnation",new SwordMetaData(74, 1.5), 50000000, 1);
    public static final Item WOODEN_GREATSWORD = new Item("Wooden greatsword",new TwoHandedSword(15, 3, 2), 30, 1);
    public static final Item STONE_GREATSWORD = new Item("Stone greatsword",new TwoHandedSword(18, 3, 4), 45, 1);
    public static final Item STEEL_GREATSWORD = new Item("Steel greatsword",new TwoHandedSword(23, 3, 5), 65, 1);
    public static final Item GREATSWORD_OF_DAMNATION = new Item("Greatsword of Damnation",new TwoHandedSword(112, 3, 34), 50000000, 1);
    public static final Item WOODEN_HALBERD = new Item("Wooden halberd",new PolearmMetaData(13, 2.3, 3), 30, 1);
    public static final Item STONE_HALBERD = new Item("Stone halberd",new PolearmMetaData(16, 2.3, 5), 45, 1);
    public static final Item STEEL_HALBERD = new Item("Steel halberd",new PolearmMetaData(20, 2.3, 7), 65, 1);
    public static final Item NAGINATA = new Item("Naginata",new PolearmMetaData(94, 2.3, 39), 50000000, 1);
    public static final Item FEATHER = new Item("Feather",null, 3, 100);
    public static final Item RUNIC_STONE = new Item("Runic stone",null, 5, 100);
    public static final Item COIN = new Item("Coin",new ValuableItemMetaData(), 1, 100);

    private Item(String name, MetaData metaData, int shopValue, int stackSize) {
        this.name = name;
        this.metaData = metaData;
        this.shopValue = shopValue;
        this.stackSize = stackSize;
    }

    static abstract class MetaData {

    }

    @EqualsAndHashCode(callSuper = true)
    @Value
    private static class ValuableItemMetaData extends MetaData {

    }

    @EqualsAndHashCode(callSuper = true)
    @Value
    private static class ArmorMetaData extends MetaData {
        private int armor;
    }

    @EqualsAndHashCode(callSuper = true)
    @Value
    private static class SwordMetaData extends MetaData {
        private int physicalDamage;
        private double attackSpeed;
    }

    @EqualsAndHashCode(callSuper = true)
    @Value
    private static class TwoHandedSword extends MetaData {
        private int physicalDamage;
        private double attackSpeed;
        private int defense;
    }

    @EqualsAndHashCode(callSuper = true)
    @Value
    private static class ShieldMetaData extends MetaData {
        private int defense;
    }

    @EqualsAndHashCode(callSuper = true)
    @Value
    private static class PolearmMetaData extends MetaData {
        private int physicalDamage;
        private double attackSpeed;
        private int defense;
    }

    @EqualsAndHashCode(callSuper = true)
    @Value
    static class StaffMetaData extends MetaData {
        private int magicDamage;
        private double attackSpeed;
    }

    @EqualsAndHashCode(callSuper = true)
    @Value
    static class RodMetaData extends MetaData {
        private int magicDamage;
        private double attackSpeed;
    }

    @EqualsAndHashCode(callSuper = true)
    @Value
    static class OffensiveRuneMetaData extends MetaData {
        private int magicDamage;
        private int areaOfEffect;
    }

    @EqualsAndHashCode(callSuper = true)
    @Value
    static class HealingRuneMetaData extends MetaData {
        private int restoredHitPoints;
    }
}
