package com.gmail.masakiyoomi.craftocalypse2d.equipment;

import com.gmail.masakiyoomi.craftocalypse2d.entity.Entity;
import com.gmail.masakiyoomi.craftocalypse2d.map.Position;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.NonFinal;

@EqualsAndHashCode(callSuper = true)
@Value
public class ItemEntity extends Entity {
    private ItemStack itemStack;

    public ItemEntity(ItemStack itemStack, Position position) {
        super(itemStack.getItem().getName(), position);
        this.itemStack = itemStack;
    }
}
