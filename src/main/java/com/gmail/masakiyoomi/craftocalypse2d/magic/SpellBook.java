package com.gmail.masakiyoomi.craftocalypse2d.magic;

import java.util.ArrayList;
import lombok.Value;

@Value
public class SpellBook {
    private final ArrayList<Castable> spells = new ArrayList<>();
}
