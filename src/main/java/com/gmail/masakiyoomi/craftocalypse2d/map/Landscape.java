package com.gmail.masakiyoomi.craftocalypse2d.map;

import com.gmail.masakiyoomi.craftocalypse2d.entity.Entity;
import com.gmail.masakiyoomi.craftocalypse2d.equipment.ItemEntity;
import lombok.Value;

import java.security.InvalidParameterException;
import java.util.ArrayList;

@Value
public class Landscape {
    private ArrayList<Entity> landscape;
    private final String mapName;

    public Landscape(String mapName, int size) {
        this.mapName = mapName;
        landscape = new ArrayList<Entity>(size);
    }

    public void add(Entity entity) {
        landscape.add(entity);
    }

    public Entity getEntity(int x, int y) {
        int targetX, targetY;
        for (int i = 0; i < landscape.size(); i++) {
            targetX = landscape.get(i).getPosition().getX();
            targetY = landscape.get(i).getPosition().getY();
            if (targetX == x && targetY == y)
                return landscape.get(i);
        }
        return null;
    }

    public ItemEntity getItemEntity(int x, int y) {
        int targetX, targetY;
        for (int i = 0; i < landscape.size(); i++) {
            targetX = landscape.get(i).getPosition().getX();
            targetY = landscape.get(i).getPosition().getY();
            if (targetX == x && targetY == y)
                return (ItemEntity)landscape.get(i);
        }
        return null;
    }

    public boolean removeEntity(Entity entity){
        try{
            landscape.remove(entity);
            return true;
        } catch (InvalidParameterException ex){
            ex.getMessage();
            return false;
        }
    }
}