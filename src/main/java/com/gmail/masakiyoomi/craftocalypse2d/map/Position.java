package com.gmail.masakiyoomi.craftocalypse2d.map;

import lombok.Value;

@Value
public class Position {
    private final int x, y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }
}