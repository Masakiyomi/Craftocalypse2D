package com.gmail.masakiyoomi.craftocalypse2d;

import com.gmail.masakiyoomi.craftocalypse2d.entity.Player;
import com.gmail.masakiyoomi.craftocalypse2d.equipment.Equipment;
import com.gmail.masakiyoomi.craftocalypse2d.equipment.ItemStack;
import com.gmail.masakiyoomi.craftocalypse2d.map.Landscape;
import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.security.InvalidParameterException;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import lombok.Getter;

public class GraphicalGameInterface extends JFrame implements GameInterface {
    public final int USER_WIDTH = Toolkit.getDefaultToolkit().getScreenSize().width;
    public final int USER_HEIGHT = Toolkit.getDefaultToolkit().getScreenSize().height;
    @Getter
    private Landscape landscape;
    public static final int guiViewRange = 10; // GUI is a rectangle of length 2*guiViewRange + 1
    private Player player;
    private ImageIcon playerImageIcon = new ImageIcon("assets/player01.png");
    private ImageIcon wooden_swordImageIcon = new ImageIcon("assets/sword.png");
    private ImageIcon wooden_shieldImageIcon = new ImageIcon("assets/shield.png");
    private ImageIcon coinImageIcon = new ImageIcon("assets/coin.png");
    private AnimationPanel animationPanel = new AnimationPanel();
    private DefaultListModel itemStacksList = new DefaultListModel();
    private JList equipmentList = new JList(itemStacksList);
    private JScrollPane equipmentPane = new JScrollPane(equipmentList);

    public GraphicalGameInterface(Player player) {
        this.player = player;
        setGuiEq(player.getEquipment());
        this.setVisible(true);
        this.setDefaultCloseOperation(3);
        this.setTitle("To jest kurwa dramat");
        this.setSize(USER_WIDTH / 2, USER_HEIGHT / 2);
        this.setLocation(USER_WIDTH / 4, USER_HEIGHT / 4);
        this.getContentPane().add(animationPanel, BorderLayout.CENTER);
        this.getContentPane().add(equipmentPane, BorderLayout.EAST);
    }

    public GraphicalGameInterface(Player player, Landscape landscape) {
        this(player);
        this.landscape = landscape;
    }

    public boolean setGuiLandscape(Landscape landscape) {
        landscape = landscape;
        return true;
    }

    public boolean setGuiEq(Equipment equipment) {
        for (ItemStack item : equipment.getEquipment()) {
            if (item != null) {
                itemStacksList.addElement(item.getItem().getName());
            }
        }
        return true;
    }

    @Override
    public void processInput() {

    }

    @Override
    public void updateGameObjects() {
        itemStacksList.clear();
        setGuiEq(player.getEquipment());
    }

    @Override
    public void render() {
        repaint();
    }

    class AnimationPanel extends JPanel {
        public void paint(Graphics g) {
            super.paint(g);
            g.drawImage(playerImageIcon.getImage(), USER_HEIGHT / 2 - 16, USER_WIDTH / 2 - 16, null);
        }
    }
}