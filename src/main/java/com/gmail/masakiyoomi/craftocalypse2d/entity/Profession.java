package com.gmail.masakiyoomi.craftocalypse2d.entity;

public class Profession {

    class Guardian {  //Loads of hitpoints, high physical and average magical defense, typical tank;

    }

    class Warlord {   //High melee damage, but weaker than Guardian. Maybe dual wielding?

    }

    class Elementalist {  //Kind of a sorcerer - casts magic spells to deal tons of dmg

    }

    class Necromancer {   //Few offensive spells, some curses, ability to summon undead creatures

    }

    class ElementWeaver {   //high magic defense, the best physical defense of all magical classes; has a few offensive spells

    }

    class Marksman {  //Bows, crossbows, throwable etc.

    }

}
