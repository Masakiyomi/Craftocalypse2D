package com.gmail.masakiyoomi.craftocalypse2d.entity;

import com.gmail.masakiyoomi.craftocalypse2d.equipment.Equipment;
import com.gmail.masakiyoomi.craftocalypse2d.map.Position;
import lombok.Getter;
import lombok.Setter;

public abstract class LivingEntity extends Entity {
    @Getter
    @Setter
    private int hitPoints;
    @Getter
    private final Equipment equipment;
    @Getter
    @Setter
    private int level = 1;

    public LivingEntity(String name, Equipment equipment, int hitPoints) {
        this(name, new Position(0, 0), equipment, hitPoints);
    }

    public LivingEntity(String name, Equipment equipment, int hitPoints, int level) {
        this(name, new Position(0, 0), equipment, hitPoints, level);
    }

    public LivingEntity(String name, Position position, Equipment equipment, int hitPoints, int level) {
        this(name, position, equipment, hitPoints);
        this.level = level;
    }

    public LivingEntity(String name, Position position, Equipment equipment, int hitPoints) {
        super(name, position);
        this.equipment = equipment;
        this.hitPoints = hitPoints;
    }
}
