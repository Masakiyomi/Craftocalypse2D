package com.gmail.masakiyoomi.craftocalypse2d.entity;

import com.gmail.masakiyoomi.craftocalypse2d.equipment.Equipment;
import com.gmail.masakiyoomi.craftocalypse2d.map.Position;

public class Necromancer extends Monster {
    public Necromancer(int coordinateX, int coordinateY) {
        super("Necromancer", new Position(coordinateX, coordinateY), new Equipment(30), 1,80);
    }
}
