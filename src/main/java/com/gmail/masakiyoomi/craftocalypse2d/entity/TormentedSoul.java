package com.gmail.masakiyoomi.craftocalypse2d.entity;

import com.gmail.masakiyoomi.craftocalypse2d.equipment.Equipment;
import com.gmail.masakiyoomi.craftocalypse2d.map.Position;

public class TormentedSoul extends Monster {
    public TormentedSoul(int coordinateX, int coordinateY) {
        super("Tormented Soul", new Position(coordinateX, coordinateY), new Equipment(50), 1, 340);
    }
}
