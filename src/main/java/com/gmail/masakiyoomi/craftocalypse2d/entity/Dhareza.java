package com.gmail.masakiyoomi.craftocalypse2d.entity;

import com.gmail.masakiyoomi.craftocalypse2d.equipment.Equipment;
import com.gmail.masakiyoomi.craftocalypse2d.map.Position;

public class Dhareza extends Monster {
    public Dhareza(int coordinateX, int coordinateY) {
        super("Dhareza", new Position(coordinateX, coordinateY), new Equipment(50), 12700, 100);
    }
}
