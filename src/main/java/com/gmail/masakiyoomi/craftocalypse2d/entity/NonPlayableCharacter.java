package com.gmail.masakiyoomi.craftocalypse2d.entity;

import com.gmail.masakiyoomi.craftocalypse2d.equipment.Equipment;
import com.gmail.masakiyoomi.craftocalypse2d.map.Position;

public class NonPlayableCharacter extends Human {
    public NonPlayableCharacter(String name, Equipment equipment, int hitPoints) {
        super(name, equipment, hitPoints);
    }

    public NonPlayableCharacter(String name, Equipment equipment, int hitPoints, int level) {
        super(name, equipment, hitPoints, level);
    }

    public NonPlayableCharacter(String name, Position position, Equipment equipment, int hitPoints, int level) {
        super(name, position, equipment, hitPoints, level);
    }

    public NonPlayableCharacter(String name, Position position, Equipment equipment, int hitPoints) {
        super(name, position, equipment, hitPoints);
    }
}
