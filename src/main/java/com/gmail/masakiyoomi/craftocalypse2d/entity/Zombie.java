package com.gmail.masakiyoomi.craftocalypse2d.entity;

import com.gmail.masakiyoomi.craftocalypse2d.equipment.Equipment;
import com.gmail.masakiyoomi.craftocalypse2d.map.Position;
import lombok.EqualsAndHashCode;
import lombok.Value;

@EqualsAndHashCode(callSuper = true)
@Value
public class Zombie extends Monster {
    public Zombie(int coordinateX, int coordinateY) {
        super("Zombie", new Position(coordinateX, coordinateY), new Equipment(10), 1, 40);
    }
}
