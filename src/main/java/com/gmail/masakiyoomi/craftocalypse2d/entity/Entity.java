package com.gmail.masakiyoomi.craftocalypse2d.entity;

import com.gmail.masakiyoomi.craftocalypse2d.map.Position;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public abstract class Entity {
    @Getter
    private final String name;
    @Getter
    @Setter
    private Position position;

    public Entity(String name) {
        this(name, new Position(0, 0));
    }
}
