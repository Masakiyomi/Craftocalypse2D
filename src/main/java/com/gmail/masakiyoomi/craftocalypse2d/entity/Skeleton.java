package com.gmail.masakiyoomi.craftocalypse2d.entity;

import com.gmail.masakiyoomi.craftocalypse2d.equipment.Equipment;
import com.gmail.masakiyoomi.craftocalypse2d.map.Position;
import lombok.EqualsAndHashCode;
import lombok.Value;

@EqualsAndHashCode(callSuper = true)
@Value
public class Skeleton extends Monster {
    public Skeleton(int coordinateX, int coordinateY) {
        super("Skeleton", new Position(coordinateX, coordinateY), new Equipment(10), 1, 20);
    }
}
