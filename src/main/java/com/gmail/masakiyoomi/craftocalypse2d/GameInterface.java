package com.gmail.masakiyoomi.craftocalypse2d;

public interface GameInterface {
    void processInput();
    void updateGameObjects();
    void render();
}
