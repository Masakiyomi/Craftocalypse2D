# Craftocalypse 2D



## Description



## Running

Here is the description how to run this game on your machine.

### Cloning the repo 

    git clone https://gitlab.com/Masakiyomi/Craftocalypse2D.git
    
### Building 

    gradle build
    
### Exporting

    gradle jar
    
### Executing

    java -jar build/libs/Craftocalypse2D-1.0.0-SNAPSHOT.jar # on Linux
    java -jar build\libs\Craftocalypse2D-1.0.0-SNAPSHOT.jar # on Windows
    
## Additional information

None at the moment